import { v4 as uuidv4 } from 'uuid';

let users = []

export const createUser = (req, res) => {
    console.log('POST ROUTE REACHED');

    const userInput = req.body;

    const userId = uuidv4();

    const userWithId = { ... userInput, id: userId };

    users.push(userWithId);
    

    res.send(`User with the name ${userInput.firstName} added`);
}

export const getUsers = (req, res) => {
    console.log(users);

    res.send(users);
}

export const getUser = (req, res) => {
    const { id } = req.params;

    const foundUser = users.find((user) => user.id === id);

    res.send(foundUser);
}

export const deleteUser = (req, res) => {
    const { id } = req.params;    

    users = users.filter((user) => user.id !== id);

    res.send(`Users with the id ${id} deleted`);
}

export const editUser = (req, res) => {
    const { id } = req.params;
    const { firstName, lastName, age } = req.body;

    const userToBeUpdated = users.find((user) => user.id === id);

    if (firstName) {
        userToBeUpdated.firstName = firstName;        
    }

    if (lastName) {
        userToBeUpdated.lastName = lastName;
    }

    if (age) {
        userToBeUpdated.age = age;
    }

    res.send(`User with the id ${id} has been updated`);
}